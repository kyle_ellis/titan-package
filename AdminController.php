<?php

namespace Extensions\Kylemassacre\Extensionpackager;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use PbbgIo\TitanFramework\Extensions;

class AdminController extends Controller
{


    public function index(): View
    {
        $extensions = Extensions::all();

        return \view('extensionpackager::admin.index', compact('extensions'));

    }


    public function packageExtension(Request $request)
    {
        $extension = Extensions::where('id', $request->input('extension'))->first();

        $path = strtolower(base_path(str_replace('\\', '/', $extension->namespace)));

        $contents = collect();

        $files = File::allFiles($path);

        foreach ($files as $file) {
            $contents->add([
                'file_name' => $file->getRelativePathname(),
                'file_content' => $file->getContents(),
            ]);
        }

        $package = [
            'name' => $extension->slug,
            'data' => $contents->toArray(),
        ];

        $download = gzcompress(serialize($package), 9);

        return response()->titanFile($download, $extension->slug);
    }

    public function uploadExtension(Request $request)
    {
        $installLocation = null;

        $dir = base_path('extensions/');

        $data = $request->file('extension')->get();

        $package = unserialize(gzuncompress($data));

        $fileName = explode('-', $package['name']);

        if (File::isDirectory($dir.$fileName[0].'/'.$fileName[1])) {
            File::deleteDirectory($dir.$fileName[0].'/'.$fileName[1]);
//            File::deleteDirectories($dir . $package['name']);
        }

        if (File::makeDirectory($dir.$fileName[0].'/'.$fileName[1], 0755, true, true)) {
            $installLocation = $dir.$fileName[0].'/'.$fileName[1].'/';
        } else {
            throw new \Exception('Unable to create a directory at '.$dir.$package['name']);
        }
        foreach (\Arr::get($package, 'data') as $file => $contents) {

            $filePath = $installLocation.\Arr::get($contents, 'file_name');

            $parts = explode('/', $filePath);
            $file = array_pop($parts);
            $dir = '';
            foreach ($parts as $part) {
                if (!is_dir($dir .= "/$part")) {
                    mkdir($dir);
                }
            }
            file_put_contents("$dir/$file", \Arr::get($contents, 'file_content'));
        }

        flash()->success('Extensions successfully uploaded');

        return redirect()->back();
    }

}

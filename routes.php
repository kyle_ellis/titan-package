<?php
Route::group([
    'prefix' => 'extensions',
    'namespace' => 'Extensions\Kylemassacre\Extensionpackager',
    'middleware' => ['web', 'auth', 'permission:admin', 'update_last_move'],
], function () {
    Route::post('package/extension/', 'AdminController@packageExtension')->name('admin.package.extension');
    Route::post('upload/extension/', 'AdminController@uploadExtension')->name('admin.upload.extension');
});
//abort()

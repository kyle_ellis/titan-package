@extends('titan::layouts.admin')

@section('page')
    <div class="card shadow mb-4">
        {!! \Form::open()->route('admin.package.extension') !!}
        <div class="card-body">
            <h2>Extensions</h2>
            <div class="form-group">
                {!!Form::select('extension', 'Select Extension')->options($extensions, 'slug', 'id')!!}
            </div>
        </div>
    </div>

    <div class="card shadow">
        <div class="card-body">
            {!!Form::submit("Package")!!}
            {!! \Form::close() !!}
        </div>
    </div>

    <div class="card shadow mb-4">
        {!! \Form::open()->route('admin.upload.extension')->multipart() !!}
        <div class="card-body">
            <h2>Extensions</h2>
            <div class="form-group">
                {!!Form::file('extension', '.titan Extension')!!}
            </div>
        </div>
    </div>

    <div class="card shadow">
        <div class="card-body">
            {!!Form::submit("Upload")!!}
            {!! \Form::close() !!}
        </div>
    </div>
@endsection

<?php
namespace Extensions\Kylemassacre\Extensionpackager;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'extensionpackager');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        \Response::macro('titanFile', function($content, $fileName) {
            $headers = [
                'Content-type'        => 'application/x-binary',
                'Content-Disposition' => 'attachment; filename='."{$fileName}" .'.titan',
            ];

            return \Response::make($content, 200, $headers);
        });
    }

    public function register()
    {

    }

}
